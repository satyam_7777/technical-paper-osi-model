
># **OSI Model**
## Introduction
**The Open Systems Interconnection (OSI)** model consists of seven layers that computer systems use to communicate over a network. It has been developed by ISO – ‘International Organization of Standardization‘, in the year 1984. The OSI Model provides a platform for various computer systems to be ready to communicate with each other.

The OSI models are often seen as a universal language for computer networking. It’s based on the concept of breaking-up up a communication system into seven meaningful layers, each layer stacked upon the last.
#### These seven layers are named as:
![text](https://www.imperva.com/learn/wp-content/uploads/sites/13/2020/02/OSI-7-layers.jpg)

Each layer of the OSI model handles a specific job and communicates with the layers above and below itself. Also, this OSI model communicates in both directions.

___
___

```Layers
Application Layer
```
This is the only layer that directly interacts with **data** from the user. Software applications like browsers and email clients believe the application layer to initiate communications. 

Application layer protocols include *HTTP*, File Transfer Protocol (FTP), Post Office Protocol (POP), *SMTP*(Simple Mail Transfer Protocol).

>![](https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/7-application-layer.svg)

```
| Actions          | Protocols
-------------------------------
| File Transfer    | FTP
| Web Surfing      | HTTP/S
| Emails           | SMTP
| Virtual Terminal | Telenets
```
___
___
```Layers
Presentation Layer
```
The presentation layer prepares data for the application layer. It defines how two devices should *encode*, **encrypt**, and _compress_ data so it is received correctly on the other end.**Compression** helps improve the speed and efficiency of communication by minimizing the quantity of data that will be transferred. 

The presentation layer takes any data transmitted by the application layer and prepares it for transmission over the session layer.

>![](https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/6-presentation-layer.svg)
___
___
```Layers
Session Layer
```
This is the layer responsible for opening and closing communication between the two devices. The time between when the communication is opened and closed is known as the **session**. The session layer ensures that the session stays open long enough to transfer all the info being exchanged, then promptly closes the session to avoid wasting resources.

The session layer also can set checkpoints during a data transfer, if the session is interrupted, devices can resume data transfer from the last checkpoint.

>### Functions of Session Layer
* **Dialog Control**
* **Token Management**
* **Synchronization**

![](https://www.studytonight.com/computer-networks/images/Figure32-1.png)
___
___
```Layers
Transport Layer
```
The transport layer is responsible for **end-to-end** communication between the two devices. This includes taking data from the session layer and breaking it up into chunks called frames before sending it to layer 3. The transport layer on the receiving device is liable for reassembling the frames into data the session layer can consume.

The transport layer is also responsible for _flow control_ and _error control_. Flow control determines an optimal speed of transmission to ensure that a sender with a fast connection doesn’t disconnect a receiver with a slow connection.
an analysis of requirements presents the characteristics of the transport layer that form the idea for a design:

1. The transport layer guarantees a reliable end-to-end connection between precisely two address spaces.
2. Data can be sent bi-directionally in the form of unstructured byte sequences of any length.
3. Different transport mechanisms should be supported.
___
___
```Layers
Network Layer
```
The network layer has two main functions. One is ending segments into network packets, and reassembling the packets on the receiving end. The other is routing packets by discovering the simplest path across a physical network. The network layer uses network addresses (typically Internet Protocol addresses) to route packets to a destination node.

>![](https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/3-network-layer.svg)

The network layer also finds the simplest physical path for the data to reach success in its destination; this is often known as **routing**.
___
___
```Layers
Data Link Layer
```
The data link layer establishes and terminates a connection between two physically-connected nodes on a network. It breaks up packets into frames and sends them from source to destination.
The data link layer takes packets from the network layer and breaks them into smaller pieces called frames.
>![](https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/2-data-link-layer.svg)
___
___
```Layers
Physical Layer
```
The physical layer is liable for the physical cable or wireless connection between network nodes. It defines the connector, the cable, or wireless technology connecting the devices, and is liable for the transmission of the data, which is just a series of 0s and 1s while taking care of bit rate control.

This is also the layer where the data gets converted into a bitstream, which is a string of 1s and 0s. The physical layer of both devices must also agree on a signal convention so that the 1s can be distinguished from the 0s on both devices.
>![](https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/1-physical-layer.svg)
___
___

## Conclusion
In the OSI model, these seven layers connected are basic building blocks of network communication. Bi-directional communication of these layers makes the OSI model more efficient and useful.

Separate protocols of each layer make the communication and data transmission between each other more efficient and interruption-free. As all these 7 layers work collaboratively to transmit the data from one person to another across the globe, all these functionalities look exciting and like a dream, But now it's all reality and growing very fast.
___
___
## References
* For pictures
  * https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/7-application-layer.svg
  * https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/6-presentation-layer.svg
  * https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/4-transport-layer.svg
  * https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/1-physical-layer.svg

* For grammar checking
  *  https://app.grammarly.com/

* For content help
  * https://www.cloudflare.com/learning/ddos/glossary/open-systems-interconnection-model-osi/
